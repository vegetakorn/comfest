<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="ComedyFemFest 2020">
    <meta name="author" content="Carlos Alvarez - Alvarez.is - blacktie.co">
    <link rel="shortcut icon" href="assets/ico/favicon.png">

    <title>ComedyFemFest 2020</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/icomoon.css">
    <link href="assets/css/animate-custom.css" rel="stylesheet">



    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>

    <script src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/modernizr.custom.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="assets/js/html5shiv.js"></script>
    <script src="assets/js/respond.min.js"></script>
    <![endif]-->
</head>

<body style="background-color: #232B8E" data-spy="scroll" data-offset="0" data-target="#navbar-main">


<div class="row">
    <div class="col-md-9">

    </div>
    <div class="col-md-3">


        <div onclick="goLiiive()" style="position:fixed;left:75.1%;">
          <img style="width: 100px; height: 100px" src="/img/Captura.png">

        </div>
    </div>
</div>


<!-- ==== HEADERWRAP ==== -->
<div id="headerwrap" id="home" name="home">
    <header class="clearfix">
        <img style="width: 30%; height: 30%" src="/img/oie_transparent.png">
        <p>¡Bienvenidas personas!</p>
        <p>¡Bienvenidos humanos!</p>
        <p>¡Bienvenido sea todo el universo!</p>


    </header>
</div><!-- /headerwrap -->


<div style="background: #A3ECF3; height: 5%">

</div>
<!-- ==== ABOUT ==== -->

<div style="background-color: #232B8E; width: 100%" class="container" id="about" name="about">
    <div class="row ">
        <br>
        <h1 style="color: #F48259" class="centered"><strong>Somos el primer festival de Stand Up femenino en México. </strong></h1>
        <hr>

        <div style="color: #F48259;text-align: center" class="col-lg-offset-2 col-lg-8">
            <strong> Hecho por mujeres: Comediantes, productoras, diseñadoras... bueno sólo una diseñadora.
                </strong>

<br style="height: 50px">

                <strong>Más de 35 mujeres listas para que usted muera de la risa. (no literal, o sea no se va a morir [pero si quiere hágase un examen del corazón antes de venir])
                </strong>
            <br><br>

        </div><!-- col-lg-6 -->


    </div><!-- row -->
</div><!-- container -->

<!-- ==== SECTION DIVIDER1 -->
<section  style="background-color: #A3ECF3;" class="section-divider textdivider divider1">
    <div  class="container">

    </div><!-- container -->
</section><!-- section -->

<div style="background: #232B8E; height: 5%">

</div>
<!-- ==== SERVICES ==== -->
<div style="background-color: #232B8E; width: 100%; " class="container" id="services" name="services">
    <br>
    <br>
    <div class="row">
        <h2 style="color: #F48259;text-align: center"  class="centered"><strong>¡Puro poder femenino bien chistoso! </strong></h2>
        <hr>
        <br>
        <div style="color: #F48259;text-align: center" class="col-lg-offset-2 col-lg-8">
            <p><strong>Tres días de comedia para todo tipo de humor: blanco, negro y todos los colores. ¡Puedes ser hombre, mujer o quimera, lo vas a disfrutar de cualquier forma!
                </strong>
            </p>
            <p><strong>Ven un día si te caes bien, ven dos días y a nosotras nos vas a caer mejor.
                    Pero si vienes tres, ¡nos has caído del cielo! </strong></p>

            <h4 style="color: #F48259"><strong>Tenemos descuentos para que te animes, marchante:  </strong></h4>

        </div><!-- col-lg -->
    </div><!-- row -->

    <div style="color: #F48259;text-align: center" class="row">
        <div class="col-lg-4 callout">

            <h2 style="color: #F48259">$350</h2>
            <p><strong>1 día</strong> </p>
        </div><!-- col-lg-4 -->

        <div class="col-lg-4 callout">

            <h2 style="color: #F48259">$550</h2>
            <p><strong>2 días</strong> </p>
        </div><!-- col-lg-4 -->

        <div class="col-lg-4 callout">

            <h2 style="color: #F48259">$650</h2>
            <p><strong>3 días</strong> </p>
        </div><!-- col-lg-4 -->
    </div><!-- row -->
</div><!-- container -->


<!-- ==== SECTION DIVIDER2 -->
<section style="background: #fff" class="section-divider textdivider divider2">
    <div class="container">
        <a href="http://bit.ly/ComedyFemFest2020" target="_blank">  <img style="" src="/img/index.png"></a>
        <hr>
        <p style="color: #232B8E" ><strong>¡Compra tus boletos aquí!</strong></p>
    </div><!-- container -->
</section><!-- section -->

<!-- ==== TEAM MEMBERS ==== -->
<div style="background-color: #A3ECF3; width: 100%" class="container" id="team" name="team">
    <br>
    <div class="row white centered">
        <h1 style="color: #121345" class="centered"><strong>ALGUNAS DE NUESTRAS STANDUPERAS CONFIRMADAS.</strong></h1>
        <hr>
        <br>
        <br>
        <div class="col-lg-3 centered">
            <img class="img img-circle" src="/img/staff/1.jpg" height="250px" width="250px" alt="">
            <br>
            <h4 style="color: #121345" ><b>Gloria Rodríguez</b></h4>
            <a href="https://instagram.com/gloriadepie?igshid=4beruviirfzk" target="_blank" class="icon icon-instagram"></a>
        </div><!-- col-lg-3 -->
        <div class="col-lg-3 centered">
            <img class="img img-circle" src="/img/staff/2.jpg" height="250px" width="250px" alt="">
            <br>
            <h4 style="color: #121345" ><b>Kikis</b></h4>
            <a href="https://instagram.com/soylakikis?igshid=vbw7oif45v"  target="_blank"  class="icon icon-instagram"></a>
        </div><!-- col-lg-3 -->
        <div class="col-lg-3 centered">
            <img class="img img-circle" src="/img/staff/3.jpeg" height="250px" width="250px" alt="">
            <br>
            <h4 style="color: #121345" ><b>Alejandra Ley</b></h4>
            <a href="https://instagram.com/alejandraleytv?igshid=qxphe934ejtu"   target="_blank" class="icon icon-instagram"></a>
        </div><!-- col-lg-3 -->
        <div class="col-lg-3 centered">
            <img class="img img-circle" src="/img/staff/4.jpg" height="250px" width="250px" alt="">
            <br>
            <h4 style="color: #121345" ><b>Ana Julia Yeyé</b></h4>
            <a href="https://instagram.com/anajuliayeye?igshid=ycbm1zz8pd9f"   target="_blank" class="icon icon-instagram"></a>
        </div><!-- col-lg-3 -->

        <div class="col-lg-3 centered">
            <img class="img img-circle" src="/img/staff/6.jpg" height="250px" width="250px" alt="">
            <br>
            <h4 style="color: #121345" ><b>Gaby Llanas</b></h4>
            <a href="https://instagram.com/lagabyllanas?igshid=w1t1at9xmsnl"   target="_blank" class="icon icon-instagram"></a>
        </div><!-- col-lg-3 -->
        <div class="col-lg-3 centered">
            <img class="img img-circle" src="/img/staff/7.jpg" height="250px" width="250px" alt="">
            <br>
            <h4 style="color: #121345" ><b>La Bea</b></h4>
            <a href="https://instagram.com/labeastandup?igshid=wxz86fh30mm4"   target="_blank" class="icon icon-instagram"></a>
        </div><!-- col-lg-3 -->
        <div class="col-lg-3 centered">
            <img class="img img-circle" src="/img/staff/9.jpg" height="250px" width="250px" alt="">
            <br>
            <h4 style="color: #121345" ><b>Coral Echeverría</b></h4>
            <a href="https://www.instagram.com/echeverriacoral/?hl=es-la"   target="_blank" class="icon icon-instagram"></a>
        </div><!-- col-lg-3 -->
        <div class="col-lg-3 centered">
            <img class="img img-circle" src="/img/staff/10.jpg" height="250px" width="250px" alt="">
            <br>
            <h4 style="color: #121345" ><b>Eva María Beristain</b></h4>
            <a href="https://instagram.com/misstercermundo?igshid=2y48fzdfbxp3"   target="_blank" class="icon icon-instagram"></a>
        </div><!-- col-lg-3 -->
        <div class="col-lg-3 centered">
            <img class="img img-circle" src="/img/staff/11.jpg" height="250px" width="250px" alt="">
            <br>
            <h4 style="color: #121345" ><b>Caro Campos</b></h4>
            <a href="https://instagram.com/carocamppos?igshid=ozqrur1ygr6k"   target="_blank" class="icon icon-instagram"></a>
        </div><!-- col-lg-3 -->
        <div class="col-lg-3 centered">
            <img class="img img-circle" src="/img/staff/12.jpg" height="250px" width="250px" alt="">
            <br>
            <h4 style="color: #121345" ><b>Jey Aderith</b></h4>
            <a href="https://www.instagram.com/soylajey/?hl=es-la"   target="_blank" class="icon icon-instagram"></a>
        </div><!-- col-lg-3 -->
        <div class="col-lg-3 centered">
            <img class="img img-circle" src="/img/staff/13.jpg" height="250px" width="250px" alt="">
            <br>
            <h4 style="color: #121345" ><b>Julia Tello</b></h4>
            <a href="https://instagram.com/akajuliatello?igshid=1pvd49rdgyk5m"   target="_blank" class="icon icon-instagram"></a>
        </div><!-- col-lg-3 -->
        <div class="col-lg-3 centered">
            <img class="img img-circle" src="/img/staff/14.jpg" height="250px" width="250px" alt="">
            <br>
            <h4 style="color: #121345" ><b>Alexa Zuart</b></h4>
            <a href="https://instagram.com/soymuynecia?igshid=cec8gjyvtq7w"   target="_blank" class="icon icon-instagram"></a>
        </div><!-- col-lg-3 -->
        <div class="col-lg-3 centered">
            <img class="img img-circle" src="/img/staff/15.jpg" height="250px" width="250px" alt="">
            <br>
            <h4 style="color: #121345" ><b>Marcela Lecuona</b></h4>
            <a href="https://www.instagram.com/marce_lecuona/"   target="_blank" class="icon icon-instagram"></a>
        </div><!-- col-lg-3 -->
        <div class="col-lg-3 centered">
            <img class="img img-circle" src="/img/staff/16.jpg" height="250px" width="250px" alt="">
            <br>
            <h4 style="color: #121345" ><b>Grecia Castillo</b></h4>
            <a href="https://instagram.com/lapinchigrecia?igshid=jdlpw7zsas9u"   target="_blank" class="icon icon-instagram"></a>
        </div><!-- col-lg-3 -->
        <div class="col-lg-3 centered">
            <img class="img img-circle" src="/img/staff/17.jpg" height="250px" width="250px" alt="">
            <br>
            <h4 style="color: #121345" ><b>Sara Silva</b></h4>
            <a href="https://instagram.com/sara_silvaman?igshid=lu7329feu5vx"   target="_blank" class="icon icon-instagram"></a>
        </div><!-- col-lg-3 -->

        <div class="col-lg-3 centered">
            <img class="img img-circle" src="/img/staff/20.jpeg" height="250px" width="250px" alt="">
            <br>
            <h4 style="color: #121345" ><b>Alexis de Anda</b></h4>
            <a href="https://www.instagram.com/alexisdeonda/?hl=es-la"   target="_blank" class="icon icon-instagram"></a>
        </div><!-- col-lg-3 -->
        <div class="col-lg-3 centered">
            <img class="img img-circle" src="/img/staff/21.jpeg" height="250px" width="250px" alt="">
            <br>
            <h4 style="color: #121345" ><b>Susana Heredia</b></h4>
            <a href="https://www.instagram.com/susyheredia/?hl=es"   target="_blank" class="icon icon-instagram"></a>
        </div><!-- col-lg-3 -->
        <div class="col-lg-3 centered">
            <img class="img img-circle" src="/img/staff/22.jpeg" height="250px" width="250px" alt="">
            <br>
            <h4 style="color: #121345" ><b>Kathia Yamanaka </b></h4>
            <a href="https://www.instagram.com/kathiay/?hl=es-la"   target="_blank" class="icon icon-instagram"></a>
        </div><!-- col-lg-3 -->
        <div class="col-lg-3 centered">
            <img class="img img-circle" src="/img/staff/23.jpeg" height="250px" width="250px" alt="">
            <br>
            <h4 style="color: #121345" ><b>Adriana Chávez </b></h4>
            <a href="https://instagram.com/adria_chavez?igshid=1bdl4t3s2dwq0"   target="_blank" class="icon icon-instagram"></a>
        </div><!-- col-lg-3 -->
        <div class="col-lg-3 centered">

        </div><!-- col-lg-3 -->

        <div class="col-lg-3 centered">
            <img class="img img-circle" src="/img/staff/24.jpeg" height="250px" width="250px" alt="">
            <br>
            <h4 style="color: #121345" ><b>Ophelia Pastrana </b></h4>
            <a href="https://www.instagram.com/ophcourse/"   target="_blank" class="icon icon-instagram"></a>
        </div><!-- col-lg-3 -->

    </div><!-- row -->
</div><!-- container -->

<div style="background: #A3ECF3; height: 10%">

</div>

<!-- ==== GREYWRAP ==== -->

    <div style="background-color: #A3ECF3; width: 100%; text-align: center; align-content: center "   >
        <img style="align-content: center"  width="100%" class="img-responsive" src="/img/index.jpg" align="center">
    </div>
    <br>
    <br>

<!-- ==== SECTION DIVIDER6 ==== -->
<section style="background-color: #232B8E" class="section-divider textdivider divider6">
    <div  class="container">
        <img style="width: 70%; height: 70%" src="/img/5.png">
        <hr>
        <p>Avenida Cuauhtémoc 19</p>
        <p>55 3498 2309</p>
        <p>
            <a class="icon icon-instagram" target="_blank" href="https://www.instagram.com/boomstandup/"></a>
            |
            <a class="icon icon-facebook"  target="_blank" href="https://www.facebook.com/boomstandup/"></a>
            |
            <a class="icon icon-map"  target="_blank" href="https://www.google.com/maps/place/BOOM+Stand+Up+Bar/@19.4249023,-99.1539874,15z/data=!4m5!3m4!1s0x0:0xd8f605dd851ccda6!8m2!3d19.4249023!4d-99.1539874"></a>
        </p>
    </div><!-- container -->
</section><!-- section -->

<div style="background-color: #F48259; width: 100%" class="container" id="contact" name="contact">
    <div class="row">
        <br>
        <h1 style="color: #121345" class="centered"><strong>¡Te esperamos!</strong></h1>
        <hr>
        <br>
        <br>
        <div class="col-lg-4">
            <h3 style="color: #121345"><strong>¡Contáctanos!</strong></h3>
            <p style="color: #121345">
<strong>
                <span class="icon icon-envelop"></span> hola@comedyfemfest.com<br/>
                <span class="icon icon-instagram"></span> <a style="color: #A3ECF3" target="_blank" href="https://www.instagram.com/comedyfemfest.mx/?hl=es-la"> @comedyfemfest.mx </a> <br/>
                <span class="icon icon-facebook"></span> <a style="color: #A3ECF3" target="_blank" href="https://www.facebook.com/Comedy-Fem-Fest-104195337754707/"> ComedyFemFest</a> <br/>

</strong>
            </p>
        </div><!-- col -->

        <div class="col-lg-4">
            <h3 style="color: #121345"><strong>¡Mensajeanos!</strong></h3>
            <p style="color: #121345"><strong>Si quieres saber mas, déjanos tu mensaje</strong></p>
            <p>
            <form class="form-horizontal" role="form">
                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-4 control-label"></label>
                    <div class="col-lg-10">
                        <input style="color: #121345" type="email" class="form-control" id="inputMail" placeholder="Correo Electrónico">
                    </div>
                </div>
                <div class="form-group">
                    <label for="text1" class="col-lg-4 control-label"></label>
                    <div class="col-lg-10">
                        <input style="color: #121345" type="text" class="form-control" id="inputMsj" placeholder="Deja tu mensaje">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-10">
                        <button type="button" onclick="sendMsj()" class="btn btn-success">Enviar</button>
                    </div>
                </div>
            </form><!-- form -->
            </p>
        </div><!-- col -->

        <div class="col-lg-4" style="text-align: center">

        </div><!-- col -->

    </div><!-- row -->

</div><!-- container -->

<div id="footerwrap">
    <div class="container">
        <img class="img img-circle" src="/img/last.jpg" height="100px" width="100px" alt="">
        <h4>ComedyFemFest - Todos los Derechos Reservados 2020</h4>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->


<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/retina.js"></script>
<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="assets/js/smoothscroll.js"></script>
<script type="text/javascript" src="assets/js/jquery-func.js"></script>
<script>
    function goLiiive()
    {
        window.open('http://bit.ly/ComedyFemFest2020', '_blank');
    }
</script>
<script src="{{ asset('alert/bootbox.min.js') }}" ></script>
<script src="{{ asset('alert/bootbox.locales.js') }}" ></script>
<script>
    function sendMsj()
    {
        //bootbox.alert("Enviando mensaje...");


            if(document.getElementById('inputMail').value != "")
            {
                if( document.getElementById('inputMsj').value != "")
                {
                    bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Enviando mensaje...</div>' })
                    var url = '{{route('sendMail')}}';
                    var token = '{{csrf_token()}}';
                    $.ajax({
                        method: 'POST',
                        url: url,
                        data: {
                            mail: document.getElementById('inputMail').value,
                            msj: document.getElementById('inputMsj').value,
                            _token: token
                        }
                    })
                        .done(function(msg){
                            console.log(msg);
                            bootbox.alert("¡Mensaje enviado! pronto estaremos en contacto...");
                            document.getElementById('inputMail').value = "";
                            document.getElementById('inputMsj').value = "";
                            bootbox.hideAll();



                        });
                }else
                {
                    bootbox.alert("¡Olvidas lo más importante!, ¡Escribe todo lo que me tengas que decir!");
                }

            }else
            {
                bootbox.alert("Me gustaría poder contactarte, por lo menos deja un correo electrónico válido");
            }
    }

</script>
</body>
</html>
