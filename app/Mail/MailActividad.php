<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use DB;

class MailActividad extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($inputs)
    {
        //
        $this->inputs = $inputs;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $subject = 'Mensaje de  Visitante'  ;

        $env = $this->view('mails.actividad',['data'=> $this->inputs])->from('hola@comedyfemfest.com')
            ->bcc('hdam23@gmail.com', 'Héctor Arroyo' );
        $env->subject($subject);


        return $env;
    }
}
