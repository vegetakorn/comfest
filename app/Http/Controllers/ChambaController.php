<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Illuminate\Mail\Mailable;
use Swift_Mailer;
use App\Mail\MailActividad;
use \Swift_SmtpTransport as SmtpTransport;
use DB;
use Illuminate\Support\Facades\Session;

class ChambaController extends Controller
{
    protected $path = 'images/'; //path para pruebas locales




    public function mail(Request $request)
    {

        // Setup a new SmtpTransport instance for Gmail
        $transport = new SmtpTransport();
        $transport->setHost('mail.comedyfemfest.com');
        $transport->setPort(587);
        $transport->setEncryption('tls');
        $transport->setUsername('info@comedyfemfest.com');
        $transport->setPassword('33GdnLRfN4xdVAT');


        // Assign a new SmtpTransport to SwiftMailer
        $driver = new Swift_Mailer($transport);

        // Assign it to the Laravel Mailer
        Mail::setSwiftMailer($driver);

        $data['mail'] = $request['mail'];
        $data['msj'] = $request['msj'];

        // Send your message
        Mail::to('hola@comedyfemfest.com')->send(new MailActividad($data));
        return response()->json(['message' => "OK"] );

    }

    public function coment(Request $request)
    {

        $data['nombre'] = $request['nombre'];
        $data['coments'] = $request['coments'];
        $data['id'] = $request['id'];
        $data['msj'] = $request['msj'];
        $coments = $data['coments'] + 1;

        DB::table('blog_com')->insert([
            [
                'blog_id' =>  $data['id'],
                'comentario' =>  $data['msj'],
                'autor' =>  $data['nombre'],
                'fecha' =>  date('Y-m-d')

            ]
        ]);

        DB::table('blog')
            ->where('id', $request['id'])
            ->update([
                'coments' =>  $coments,

            ]);

        return response()->json(['message' => "OK"] );

    }

    public function contenido($id)
    {
        $data['contenido'] =   DB::table('blog')
            ->where('blog.id','=',$id)
            ->first();

        $data['comentarios'] =   DB::table('blog_com')
            ->where('blog_com.blog_id','=',$id)
            ->get();
        return view('/content')->with( $data);

    }

    public function getSignOut() {

        Session::flush();
        $data['fotos'] =   DB::table('gal_inicio')->get();
        return view('home')->with( $data);
    }
}
